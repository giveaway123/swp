const temp = document.querySelector('#temp')
const desc = document.querySelector('#desc')
const cityNameField = document.querySelector('#cityName')

const getTemp = async (cityName) => {
	const response = await fetch('/weather/?address='+cityName)
	if(response.status === 200){
		return await response.json()
	}

	else
		throw new Error("Unable to connect to the network!")
	
}

const writeToDOM = (city) => {
	temp.innerHTML = "Temperature in " + city.locationName + " is " + city.temp + "c and it feels like " + city.feelslike + "c"
	desc.innerHTML = city.desc
}

const getTempFromIP = async () => {
	const response = await fetch('https://freegeoip.app/json/')
	if(response.status = 200){
		const data = await response.json()
		const city = await getTemp(data.city)
		writeToDOM(city)

	}
	else
		throw new Error("Unable to connect to the network!")

}

getTempFromIP()

document.querySelector('#Enter').addEventListener('click',(e) => {
	e.preventDefault()
	const cityName = cityNameField.value
	temp.innerHTML = 'Loading...'
	desc.innerHTML = ''
	getTemp(cityName).then(city => {
		if(city.error)
		{
			temp.innerHTML = city.error
			desc.innerHTML = ''
		}
		else{
			writeToDOM(city)
		}
	}).catch(e => {
		temp.innerHTML = ''
		desc.innerHTML = ''
		temp.innerHTML = e
	})
})
