const path = require('path')
const express = require('express')
const request  = require('request')
const rp = require('request-promise-native')

const app = express()

const viewPath = path.join(__dirname,'./views')

app.use(express.static(viewPath))

const getWeather = async (city) => {
	try {
		const weatherJSON = await rp('http://api.weatherstack.com/current?access_key=76d3beb3117ed2ee494339a231e04a67&query='+city)
		const weather = JSON.parse(weatherJSON)
		if(weather.error){
			return {error: "Invalid city name"}
		}
		else {return {
			locationName: weather.location.name + ", " + weather.location.region,
			temp:	weather.current.temperature,
			feelslike: weather.current.feelslike,
			desc: weather.current.weather_descriptions[0]
		}
	}
	} catch (e) {
		throw new Error(e)
	}
}

const port = process.env.PORT || 3000


app.get('',(req,res) => {
	
	res.send('index')
})

app.get('/weather',(req,res) => {
	const cityName = req.query.address
	getWeather(cityName).then(city => {
		res.send(city)
	}).catch(e => {
		res.status(400).send(e)
	})
})

app.listen(port,() => {
	console.log("Connected")
})

